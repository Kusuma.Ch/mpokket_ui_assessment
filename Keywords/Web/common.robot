*** Settings ***
Resource          ../../Global/super.robot

*** Keywords ***
Launch Mobile Application
    [Arguments]    ${platform_name}
    [Documentation]    Launch Application in Android and iOS platforms.
    ...
    ...    Examples:
    ...    mobile_common.Launch Mobile Application ${PLATFORM_NAME}
    ...    mobile_common.Launch Mobile Application Android/ios
    Run Keyword If    '${platform_name}'=='Android'    AppiumLibrary.Open Application    ${APPIUM_SERVER_URL}    platformName=Android    platformVersion=${ANDROID_PLATFORM_VERSION}    deviceName=${ANDROID_DEVICE_NAME}    app=${ANDROID_APP}    automationName=${ANDROID_AUTOMATION_NAME}    noReset=${ANDROID_NO_RESET_APP}    autoAcceptAlerts=True    autoGrantPermissions=True    newCommandTimeout=${NEW_COMMAND_TIMEOUT}
    Run Keyword If    '${platform_name}'=='iOS'    AppiumLibrary.Open Application    ${APPIUM_SERVER_URL}    platformName=iOS    platformVersion=${IOS_PLATFORM_VERSION}    deviceName=${IOS_DEVICE_NAME}    app=${IOS_APP}    udid=${UDID}    bundleId=${BUNDLE_ID}    automationName=${IOS_AUTOMATION_NAME}    noReset=False    autoAcceptAlerts=False
    Run Keyword If    '${platform_name}'!='iOS' and '${platform_name}'!='Android'    Fail    Platform Name used '${platform_name}'. Please provide valid Platform Name.
    Set Appium Timeout    15s

Input Text
    [Arguments]    ${locator}    ${data}
    [Documentation]    Enters Text into textbox, and Hide the android keyboard.
    ...
    ...    Example:
    ...    mobile_common.Input Text locator text
    Run Keyword If    '${data}'!='NA'    AppiumLibrary.Input Text    ${locator}    ${data}
    Run Keyword If    '${PLATFORM_NAME}'=='Android'    AppiumLibrary.Hide Keyboard
    Run Keyword If    '${PLATFORM_NAME}'=='iOS' and '${Username}'!='NA'    AppiumLibrary.Click Element    //XCUIElementTypeButton[@name='Return']

Read TestData From Excel
    [Arguments]    ${testcaseid}    ${sheet_name}
    [Documentation]    Read TestData from excel file for required data.
    ...
    ...    Example:
    ...    Read TestData From Excel TC_01 SheetName
    ${test_prerequisite_data}    CustomLibrary.Get Ms Excel Row Values Into Dictionary Based On Key    ${TESTDATA_FOLDER}/TestData.xlsx    ${testcaseid}    ${sheet_name}
    Set Global Variable    ${test_prerequisite_data}
    [Return]    ${test_prerequisite_data}

Generate Random Email
    ${random_number}    FakerLibrary.Random Number    9    True
    ${email}    Set Variable    Test_${random_number}@mailinator.com
    [Return]    ${email}

Fail and take screenshot
    [Arguments]    ${message}
    AppiumLibrary.Capture Page Screenshot
    Fail    ${message}

Update Dynamic Value
    [Arguments]    ${locator}    ${value}
    [Documentation]    It replace the string where ever you want.
    ...
    ...    Example:
    ...    mobile_common.Update Dynamic Value locator replace_string
    ${xpath}=    Replace String    ${locator}    replaceText    ${value}
    [Return]    ${xpath}

Update Dynamic Values
    [Arguments]    ${locator}    ${value1}    ${value2}
    ${locator}=    Replace String    ${locator}    replaceText1    ${value1}
    ${xpath}=    Replace String    ${locator}    replaceText2    ${value2}
    [Return]    ${xpath}

Get control enable status
    [Arguments]    ${locator}
    ${status_name}=    AppiumLibrary.Get Element Attribute    ${locator}    content-desc
    @{statuslist}=    Split String    ${status_name}    ,
    ${status}=    Get From list    ${statuslist}    1
    ${status}=    Remove String    ${status}    ${SPACE}
    [Return]    ${status}

Launch mobile app
    AppiumLibrary.Open Application    ${APPIUM_SERVER_URL}    platformName=Android    platformVersion=${ANDROID_PLATFORM_VERSION}    deviceName=${ANDROID_DEVICE_NAME}    app=${ANDROID_APP}    automationName=${ANDROID_AUTOMATION_NAME}    noReset=${ANDROID_NO_RESET_APP}    autoAcceptAlerts=True    autoGrantPermissions=True    newCommandTimeout=${NEW_COMMAND_TIMEOUT}

Print the languages
    AppiumLibrary.Wait Until Element Is Visible    ${label.languages}    ${LONG_WAIT}    ${label.languages} Element is not visible after waiting {LONG_WAIT}
    @{languages_main_list}    Create List
    @{EmptyList}    Create List
    Set Global Variable    ${languages_main_list}    ${EmptyList}
    FOR    ${key}    IN RANGE    1    20
        ${languages_count}    Get Matching Xpath Count    ${label.languages}
        Log    ${languages_count}
        Get language name    ${languages_count}
        ${status}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${label.last_language}
        Log    ${languages_main_list}
        Exit For Loop If    ${status}==True
        AppiumExtendedLibrary.swipe down    1
    END

Get language name
    [Arguments]    ${language_count}
    FOR    ${value}    IN RANGE    1    ${language_count}+1
        ${value}    Convert To String    ${value}
        ${label.language_text.new}    Update Dynamic Value    ${label.language_text}    ${value}
        ${language_status}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${label.language_text.new}    1s
        Run Keyword If    '${language_status}'=='True'    Get each language name    ${label.language_text.new}
        log    ${full_name}
        @{languages_list}    Create List    ${full_name}
        Log    @{languages_list}
        Append To List    ${languages_main_list}    ${languages_list}
    END

Get each language name
    [Arguments]    ${locator}
    ${value}    AppiumLibrary.Get Text    ${locator}
    Set Global Variable    ${full_name}    ${value}
    Log    ${full_name}

Selecting English Language
    AppiumExtendedLibrary.swipe up    2
    AppiumLibrary.Click Element    ${label.english_language}

Select Continue button
    AppiumLibrary.Click Element    ${label.Continue}

Open The Browser and Launch the Application
    Comment    Open the browser
    Open Browser    ${url}    chrome
    Comment    Maximize the browser
    Maximize Browser Window

Search for a product
    [Arguments]    ${amazon_data}
    SeleniumLibrary.Wait Until Element Is Visible    ${textbox.search}    ${SHORT_WAIT}    Element is not visible after waiting ${SHORT_WAIT}
    Comment    selecting search bar
    SeleniumLibrary.Click Element    ${textbox.search}
    Comment    Enter search product name
    SeleniumLibrary.Input Text    ${textbox.search}    ${amazon_data}[product_name]
    SeleniumLibrary.Click Element    ${button.search}

Validating search list
    [Arguments]    ${amazon_data}
    Comment    Validating search list
    SeleniumLibrary.Element Should Contain    ${label.search_result_bar}    ${amazon_data}[product_name]

Select a product and add to cart
    [Arguments]    ${amazon_data}
    Comment    Selecting a book
    ${book_name}=    SeleniumLibrary.Get Text    ${label.book}
    SeleniumLibrary.Click Element    ${label.book}
    Set Global Variable    ${book_name}
    Switch Window    NEW
    SeleniumLibrary.Wait Until Element Is Visible    ${button.add_to_cart}    ${SHORT_WAIT}    Element is not visible after waiting ${SHORT_WAIT}
    Comment    Increasing the quantity of the selected book
    SeleniumLibrary.Select From List By Value    ${list.quantity}    ${amazon_data}[quantity]
    Comment    Add the selected book \ to the cart
    SeleniumLibrary.Click Element    ${button.add_to_cart}

Validating the cart
    SeleniumLibrary.Wait Until Element Is Visible    ${label.Add_to_cart}    ${SHORT_WAIT}    Element is not visible after waiting ${SHORT_WAIT}
    Comment    Click on cart button
    SeleniumLibrary.Click Element    ${button.cart}
    Page Should Contain    Shopping Cart
    SeleniumLibrary.Element Should Contain    ${label.cart_container}    ${book_name}
    ${total_price_single_product}=    SeleniumLibrary.Get Text    ${label.total_price}
    Set Global Variable    ${total_price_single_product}

Adding more items to the cart
    [Arguments]    ${quantity}
    FOR    ${product_number}    IN RANGE    2    4
        Switch Window    MAIN
        ${product_number}    Convert To String    ${product_number}
        ${list.book_new}    Update Dynamic Value    ${list.product}    ${product_number}
        ${book_status}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${list.book_new}    1s
        ${book_name}=    SeleniumLibrary.Get Text    ${list.book_new}
        SeleniumLibrary.Click Element    ${list.book_new}
        Switch Window    NEW
        SeleniumLibrary.Wait Until Element Is Visible    ${button.add_to_cart}    ${SHORT_WAIT}    Element is not visible after waiting ${SHORT_WAIT}
        SeleniumLibrary.Select From List By Value    ${list.quantity}    ${quantity}
        SeleniumLibrary.Click Element    ${button.add_to_cart}
    END

Validating total peice after adding products
    SeleniumLibrary.Wait Until Element Is Visible    ${label.Add_to_cart}    ${SHORT_WAIT}    Element is not visible after waiting ${SHORT_WAIT}
    SeleniumLibrary.Click Element    ${button.cart}
    Page Should Contain    Shopping Cart
    SeleniumLibrary.Element Should Contain    ${label.cart_container}    ${book_name}
    ${total_price_new}=    SeleniumLibrary.Get Text    ${label.total_price}
    Should Not Be Equal As Strings    ${total_price_single_product}    ${total_price_new}
    Set Global Variable    ${total_price_new}

Removing a product from the cart
    SeleniumLibrary.Element Should Be Visible    ${label.Delete}
    Sleep    10s
    Comment    Click on delete button
    SeleniumLibrary.Click Element    ${label.Delete}
    SeleniumLibrary.Wait Until Element Is Visible    ${label.total_price}    ${MEDIUM_WAIT}    Element is not visible after waiting ${SHORT_WAIT}
    Comment    Get the Total price
    ${total_price_after_removing_product}=    SeleniumLibrary.Get Text    ${label.total_price}
    Set Global Variable    ${total_price_after_removing_product}

Validating price after removing a product
    Comment    Validating the total price after removing one product
    Should Not Be Equal As Strings    ${total_price_after_removing_product}    ${total_price_new}
