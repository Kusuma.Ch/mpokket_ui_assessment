*** Settings ***
Library           SeleniumLibrary    run_on_failure=SeleniumLibrary.CapturePageScreenshot
Library           AppiumLibrary    run_on_failure=AppiumLibrary.CapturePageScreenshot
Library           FakerLibrary
Library           String
Library           Collections
Library           Screenshot
Library           DateTime
Library           ../Library/CustomLibrary.py
Library           ../Library/AppiumExtendedLibrary.py
Resource          common_variables.robot
Library           requests
Resource          common_variables.robot
Resource          ../ObjectRepository/labels.robot
Library           ../Library/AppiumExtendedLibrary.py
Library           ../Library/CustomLibrary.py
Resource          ../Keywords/Web/common.robot
Resource          android_variables.robot
