*** Settings ***
Suite Setup       Open The Browser and Launch the Application
Test Teardown     SeleniumLibrary.Close Browser
Resource          ../Global/super.robot

*** Variables ***

*** Test Cases ***
TC01 - 1. Search for a product. Verify the list.
    ${tcname}=    Set Variable    TC_01
    Comment    Open the browser and Launching the application
    Comment    Open The Browser and Launch the Application
    ${amazon_data}=    Read TestData From Excel    ${tcname}    Amazon
    Comment    Searching for a product
    Search for a product    ${amazon_data}
    Comment    Validating search list
    Validating search list    ${amazon_data}

TC02 - 2. Add a product to the cart. Verify that the added product is in the cart.
    ${tcname}=    Set Variable    TC_01
    Comment    Launching mobile application
    Open The Browser and Launch the Application
    ${amazon_data}=    Read TestData From Excel    ${tcname}    Amazon
    Comment    Searching for a product
    Search for a product    ${amazon_data}
    comment    Select a product and add to cart
    Select a product and add to cart    ${amazon_data}
    Comment    Validating the cart
    Validating the cart

TC03 - 3. Increase the number of items in the cart to 10. Verify that the total price changed.
    ${tcname}=    Set Variable    TC_03
    Comment    Launching mobile application
    Open The Browser and Launch the Application
    ${amazon_data}=    Read TestData From Excel    ${tcname}    Amazon
    Comment    Searching for a product
    Search for a product    ${amazon_data}
    comment    Select a product and add to cart
    Select a product and add to cart    ${amazon_data}
    Comment    Validating the cart
    Validating the cart
    Comment    Adding more items to the cart
    Adding more items to the cart    ${amazon_data}[quantity]
    Comment    Validating total peice after adding products
    Validating total peice after adding products

TC04 - 4. Remove a product from the cart. Verify the change.
    ${tcname}=    Set Variable    TC_04
    Comment    Launching mobile application
    Open The Browser and Launch the Application
    ${amazon_data}=    Read TestData From Excel    ${tcname}    Amazon
    Comment    Searching for a product
    Search for a product    ${amazon_data}
    comment    Select a product and add to cart
    Select a product and add to cart    ${amazon_data}
    Comment    Validating the cart
    Validating the cart
    Comment    Adding more items to the cart
    Adding more items to the cart    ${amazon_data}[quantity]
    Comment    Validating total peice after adding products
    Validating total peice after adding products
    Comment    Removing a product from the cart
    Removing a product from the cart
    Comment    Validating price after removing a product
    Validating price after removing a product
